<?php
    
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "lets_party";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    } 
    $sql = "select * from image_upload";
    $result = $conn->query($sql);
    ?>
    <head>
    <title>Gallary</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    
</head>
    <style>
    
    </style>
       <?php //include("nav.php");?>
        <h1 align="center">Gallary</h1><hr>
    <div class="container">
        <div class="row">
        <?php

        foreach ($result as $key => $value) {
        ?>
            
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body">
                        <a href="<?php echo 'uploads/'.$value['image_name']?>"><img  class="card-img-top" src="uploads/<?php echo $value['image_name']?>" alt="<?php echo $value['image_name']?>"></a>   
                    </div>
                </div>
            </div>
        
    <?php
}

    $conn->close();
    ?>
        </div>
    </div>
    <?php
            
        include("social_logo.php");
?>