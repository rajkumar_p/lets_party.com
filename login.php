<!DOCTYPE html>
<html lang="en">
<head>
  <title>Login</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <style>
        .container{
            width: 30%;
            margin-top:150px;
        }
        p{
        font-size:18px;
        }
    </style>
</head>
<?php //include('nav.html)
  session_start();
  if(isset($_SESSION['naam'])){
    header('location:image_upload.php');
  }else{
?>
<body>

<div class="container">
<div class="card">  
<div class="card-body">

  <h2>Login</h2>
  <form action="action.php" method="post">
    <div class="form-group">
      <label for="email">Email:</label>
      <input type="email" class="form-control"  name="email" id="email" required placeholder="Enter email" >
    </div>
    <div class="form-group">
      <label for="pws">Password:</label>
      <input type="password" class="form-control" name="pws"  id="pws" required placeholder="Enter password" maxlength="10">
    </div>
    <div class="form-group">
    <input type="submit"   class="form-control btn-info" value="login"><br>
    <p align="right">forgot <a href="change_password.php">password?</a></p>
    </div>
   
  </form>
  </div>
  </div>
</div>

</body>
</html>
  <?php } ?>